<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Test CI Application - <?php echo $title;?></title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url();?>assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url();?>assets/css/style.min.css" rel="stylesheet">
    <style type="text/css">
        
        .dropdown-menu li:hover .sub-menu {
  visibility: visible;
}

.dropdown:hover .dropdown-menu {
  display: block;
}
    </style>
</head>

<body>

    <!--Main Navigation-->
    <header>

        <!-- Navbar -->
        <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
            <div class="container">

                <!-- Brand -->
                <a class="navbar-brand waves-effect" href="https://mdbootstrap.com/material-design-for-bootstrap/" target="_blank">
                    <strong class="blue-text">MDB</strong>
                </a>

                <!-- Collapse -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- Links -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    <!-- Left -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link waves-effect" href="#">Inicio
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                                  <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">¿Quienes somos? <b class="caret"></b></a>
            <ul class="dropdown-menu">

            
              <li><a href="<?php echo base_url().'home/rector';?>">Palabras del rector</a></li>
              <li><a href="<?php echo base_url().'home/historia';?>">Nuestra historia</a></li>
              <li><a href="#">Vision</a></li>
              <li><a href="#">Filosofía</a></li>
              <li><a href="#">Isa en la actualidad</a></li>
              <li class="divider"></li>

            </ul>
          </li>

                        <li class="nav-item">
                            <a class="nav-link waves-effect" href="#" target="_blank">Programas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link waves-effect" href="#" target="_blank">Admisiones</a>
                        </li>
                                                <li class="nav-item">
                            <a class="nav-link waves-effect" href="#" target="_blank">Investigaciones</a>
                                                                            <li class="nav-item">
                            <a class="nav-link waves-effect" href="#" target="_blank">Noticias</a>
                                                                            <li class="nav-item">
                            <a class="nav-link waves-effect" href="#" target="_blank">Contactos</a>
                                                                            <li class="nav-item">
                            <a class="nav-link waves-effect" href="#" target="_blank">Biblioteca</a>
                        </li>
                    </ul>

                    <!-- Right -->
                    <ul class="navbar-nav nav-flex-icons">
                        <li class="nav-item">
                            <a href="#" class="nav-link waves-effect" target="_blank">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link waves-effect" target="_blank">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
 
                    </ul>

                </div>

            </div>
        </nav>
        <!-- Navbar -->

    </header>
    <!--Main Navigation-->

    <!--Main layout-->
    <main class="mt-5 pt-5">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->


                    <?php echo "<pre>";
                    echo $contents;
                    echo "</pre>";?>


                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
            <!--Section: Jumbotron-->

            <!--Section: Jumbotron-->

            <!--Section: Magazine v.1-->
            <section class="wow fadeIn">

                <!--Section heading-->
                <h2 class="h1 text-center my-5 font-weight-bold">Contenido de la página</h2>

                <!--Grid row-->
                <div class="row text-left">

                    <!--Grid column-->
                    <div class="col-lg-6 col-md-12">

                        <!--Image-->
                        <div class="view overlay rounded z-depth-1-half mb-3">
                            <img src="<?php echo base_url();?>/images/imagen1.jpg" class="img-fluid" alt="Sample post image">
                            <a>
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>

                        <!--Excerpt-->
                        <div class="news-data">
                            <a href="" class="light-blue-text">
                                <h6>
                                    <i class="fa fa-plane"></i>
                                    <strong> Travels</strong>
                                </h6>
                            </a>
                            <p>
                                <strong>
                                    <i class="fa fa-clock-o"></i> 20/08/2018</strong>
                            </p>
                        </div>
                        <h3>
                            <a>
                                <strong>CONOCE A ISA</strong>
                            </a>
                        </h3>
                        <p> Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime
                            placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.
                        </p>

                        <!--/Featured news-->

                        <hr>

                        <!--Small news-->
                        <div class="row">
                            <div class="col-md-3">

                                <!--Image-->
                                <div class="view overlay rounded z-depth-1">
                                    <img src="https://mdbootstrap.com/img/Photos/Others/photo8.jpg" class="img-fluid" alt="Minor sample post image">
                                    <a>
                                        <div class="mask rgba-white-slight"></div>
                                    </a>
                                </div>
                            </div>

                            <!--Excerpt-->
                            <div class="col-md-9">
                                <p class="dark-grey-text">
                                    <strong>19/08/2018</strong>
                                </p>
                                <a>Lorem ipsum dolor sit amet
                                    <i class="fa fa-angle-right float-right"></i>
                                </a>
                            </div>

                        </div>
                        <!--/Small news-->

                        <hr>

                        <!--Small news-->
                        <div class="row">
                            <div class="col-md-3">

                                <!--Image-->
                                <div class="view overlay rounded z-depth-1">
                                    <img src="https://mdbootstrap.com/img/Photos/Others/images/54.jpg" class="img-fluid" alt="Minor sample post image">
                                    <a>
                                        <div class="mask rgba-white-slight"></div>
                                    </a>
                                </div>
                            </div>

                            <!--Excerpt-->
                            <div class="col-md-9">
                                <p class="dark-grey-text">
                                    <strong>18/08/2018</strong>
                                </p>
                                <a>Soluta nobis est eligendi
                                    <i class="fa fa-angle-right float-right"></i>
                                </a>
                            </div>

                        </div>
                        <!--/Small news-->

                        <hr>

                        <!--Small news-->
                        <div class="row">
                            <div class="col-md-3">

                                <!--Image-->
                                <div class="view overlay rounded z-depth-1">
                                    <img src="https://mdbootstrap.com/img/Photos/Others/images/49.jpg" class="img-fluid" alt="Minor sample post image">
                                    <a>
                                        <div class="mask rgba-white-slight"></div>
                                    </a>
                                </div>
                            </div>

                            <!--Excerpt-->
                            <div class="col-md-9">
                                <p class="dark-grey-text">
                                    <strong>17/08/2018</strong>
                                </p>
                                <a>Voluptatem accusantium doloremque
                                    <i class="fa fa-angle-right float-right"></i>
                                </a>
                            </div>

                        </div>
                        <!--/Small news-->

                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-lg-6 col-md-12">

                        <!--Image-->
                        <div class="view overlay rounded z-depth-1-half mb-3">
                            <img src="<?php echo base_url();?>/images/imagen2.jpg" class="img-fluid" alt="Sample post image">
                            <a>
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>

                        <!--Excerpt-->
                        <div class="news-data">
                            <a href="" class="light-blue-text">
                                <h6>
                                    <i class="fa fa-plane"></i>
                                    <strong> Travels</strong>
                                </h6>
                            </a>
                            <p>
                                <strong>
                                    <i class="fa fa-clock-o"></i> 20/08/2018</strong>
                            </p>
                        </div>
                        <h3>
                            <a>
                                <strong>BIBLIOTECA</strong>
                            </a>
                        </h3>
                        <p> Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime
                            placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.
                        </p>

                        <!--/Featured news-->

                        <hr>

                        <!--Small news-->
                        <div class="row">
                            <div class="col-md-3">

                                <!--Image-->
                                <div class="view overlay rounded z-depth-1">
                                    <img src="https://mdbootstrap.com/img/Photos/Others/photo11.jpg" class="img-fluid" alt="Minor sample post image">
                                    <a>
                                        <div class="mask rgba-white-slight"></div>
                                    </a>
                                </div>
                            </div>

                            <!--Excerpt-->
                            <div class="col-md-9">
                                <p class="dark-grey-text">
                                    <strong>19/08/2018</strong>
                                </p>
                                <a>Lorem ipsum dolor sit amet
                                    <i class="fa fa-angle-right float-right"></i>
                                </a>
                            </div>

                        </div>
                        <!--/Small news-->

                        <hr>

                        <!--Small news-->
                        <div class="row">
                            <div class="col-md-3">

                                <!--Image-->
                                <div class="view overlay rounded z-depth-1">
                                    <img src="https://mdbootstrap.com/img/Photos/Others/images/51.jpg" class="img-fluid" alt="Minor sample post image">
                                    <a>
                                        <div class="mask rgba-white-slight"></div>
                                    </a>
                                </div>
                            </div>

                            <!--Excerpt-->
                            <div class="col-md-9">
                                <p class="dark-grey-text">
                                    <strong>18/08/2018</strong>
                                </p>
                                <a>Soluta nobis est eligendi
                                    <i class="fa fa-angle-right float-right"></i>
                                </a>
                            </div>

                        </div>
                        <!--/Small news-->

                        <hr>

                        <!--Small news-->
                        <div class="row">
                            <div class="col-md-3">

                                <!--Image-->
                                <div class="view overlay rounded z-depth-1">
                                    <img src="https://mdbootstrap.com/img/Photos/Others/images/44.jpg" class="img-fluid" alt="Minor sample post image">
                                    <a>
                                        <div class="mask rgba-white-slight"></div>
                                    </a>
                                </div>
                            </div>

                            <!--Excerpt-->
                            <div class="col-md-9">
                                <p class="dark-grey-text">
                                    <strong>17/08/2018</strong>
                                </p>
                                <a>Voluptatem accusantium doloremque
                                    <i class="fa fa-angle-right float-right"></i>
                                </a>
                            </div>

                        </div>
                        <!--/Small news-->

                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

            </section>
            <!--/Section: Magazine v.1-->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <?php echo $contents;?>
                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </main>
    <!--Main layout-->

    <!--Footer-->
    <footer class="page-footer text-center font-small mdb-color darken-2 mt-4 wow fadeIn">

        <!--Call to action-->

        <!--/.Call to action-->

        <hr class="my-4">

        <!-- Social icons -->
        <div class="pb-4">
            <a href="#" target="_blank">
                <i class="fa fa-facebook mr-3"></i>
            </a>

            <a href="#" target="_blank">
                <i class="fa fa-twitter mr-3"></i>
            </a>

            <a href="#" target="_blank">
                <i class="fa fa-youtube mr-3"></i>
            </a>

            <a href="#" target="_blank">
                <i class="fa fa-google-plus mr-3"></i>
            </a>

            <a href="#" target="_blank">
                <i class="fa fa-dribbble mr-3"></i>
            </a>

            <a href="#" target="_blank">
                <i class="fa fa-pinterest mr-3"></i>
            </a>

            <a href="#" target="_blank">
                <i class="fa fa-github mr-3"></i>
            </a>

            <a href="#" target="_blank">
                <i class="fa fa-codepen mr-3"></i>
            </a>
        </div>
        <!-- Social icons -->

        <!--Copyright-->
        <div class="footer-copyright py-3">
            © 2018 Copyright:
            <a href="#" target="_blank"> MDBootstrap.com </a>
        </div>
        <!--/.Copyright-->

    </footer>
    <!--/.Footer-->

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/mdb.min.js"></script>
    <!-- Initializations -->
    <script type="text/javascript">
        // Animations initialization
        new WOW().init();
    </script>
</body>

</html>