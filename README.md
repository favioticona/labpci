## Instrucciones para crear el proyecto en CI:

Requisitos: php7  
Framework: Codeigniter 3  

Lo colocamos en xampp C:\xampp\htdocs\labpci

Para ver en el navegador acceder a http://localhost/labpci/

1.Primero configuramos la URL del proyecto en config.php  

labpci/application/config/config.php  

2.Configuramos la base de datos en database.php  

labpci/application/config/database.php
### Configuración  

Nombre de la base: labpci_db
Usuario: root
Password: 


3.Configurando la plantilla en 
labpci/application/libraries/template.php


4.Configurando las vistas:

layouts:
labpci/application/views/layouts/default_layout.php  // donde se llama todos los estilos boostrap y javascript
labpci/application/views/Home.php

5.Configurando el controlador y llamando al template:
labpci/application/controllers/Home.php


### Para las preguntas:

6.Para el menu se uso dropdown de boostrap con la funcion on hover

En el menú ¿Quienes somos?
![Menu onhover](images/muestra1.png "titulo")

7.Para "Palabras del Rector":  
http://localhost/labpci/home/rector  
![Lorem](images/muestra2.png "titulo")
![Lorem2](images/muestra21.png "titulo")

8.Para "Nuestra historia"  
http://localhost/labpci/home/historia
![Lorem](images/muestra3.png "titulo")






